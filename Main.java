import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;

public class Main
{
static int[] a; // = {1, 2, 3, 4};
static int[] z = new int[100]; // viele Elemente für z bereitstellen
static int indexZ = 0;

public static void quick(int[] b, int laenge){
    int p = b[0];
    int[] l;
    int[] r;
    int anzahlZeichenL=0;
    int anzahlZeichenR=0;
    
    for(int k = 0; k < laenge; k++){
        if (b[k] < p) anzahlZeichenL++;
        if (b[k] > p) anzahlZeichenR++;
    }
    l = new int[anzahlZeichenL+1];
    r = new int[anzahlZeichenR+1];
    
    int indexArrL = 0;
    int indexArrR = 0;
    
    for(int k = 0; k < laenge; k++){
        if (b[k] < p) {
            l[indexArrL] = b[k];
            indexArrL++;
        }
        if (b[k] > p) {
            r[indexArrR] = b[k];
            indexArrR++;
        }
    }
    
    if(indexArrL > 1) quick(l, indexArrL);
    else if (indexArrL == 1) {
        z[indexZ] = l[0];
        indexZ++;
    }
    
    z[indexZ] = p;
    indexZ++;
    
    if(indexArrR > 1) quick(r, indexArrR);
    else if (indexArrR == 1) {
        z[indexZ] = r[0];
        indexZ++;
    }
    
}

public static void main (String[]args)
{
  int anzahlZeichen=0;
  Scanner in=new Scanner(System.in).useDelimiter("\n");
  // Delimiter Zeilenumbruch festlegen, denn sonst wird nach Leerzeichen nicht weiter eingelesen
  
  System.out.println ("- Quicksort-Programm -");
  System.out.println ("Geben Sie mehrere ganze Zahlen ein, die durch");
  System.out.println ("Leerzeichen voneinander getrennt sind. Z.B.: 9 2 12 3 4 6 5");
  System.out.println ("Druecken Sie dann Enter, damit die Liste sortiert wird.");
  String eingabe="";
  // eingabe += in.next();
  try
  {eingabe += in.next();}
  catch(Exception e)
  {return;}

  in.close();
  String x="";
  for (int k = 0; k < eingabe.length(); k++) {
      if (eingabe.charAt(k) == ' ') {
          anzahlZeichen++; 
      }
  }
  a = new int[anzahlZeichen+1]; // auch letztes Zeichen im Feld verfügbar machen
  int indexArr = 0;
  for (int k = 0; k < eingabe.length(); k++) {
    if (eingabe.charAt(k) == ' ') {
        a[indexArr] =  Integer.parseInt(x);
        indexArr++;
        x = "";
    }
    else x = x + eingabe.charAt(k);
  }
  a[indexArr] =  Integer.parseInt(x);
  quick(a, indexArr+1);
  System.out.println ();
  System.out.println ("Sortiertes Array: ");
  System.out.println ();  
  for (int k = 0; k < indexZ; k++) {
      System.out.print(z[k] + " "); 
  }
}
}